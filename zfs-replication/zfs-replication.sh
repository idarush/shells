#!/bin/bash

SOURCE=$1 # ожидается хост:датасет
TARGET=$2 # ожидается хост:датасет

HOST_SOURCE=`echo $SOURCE | sed 's/:/ /g' | awk '{print $1}'`	# await "local" or ip address. If "local" then ssh not use.
HOST_TARGET=`echo $TARGET | sed 's/:/ /g' | awk '{print $1}'`	# await "local" or ip address. If "local" then ssh not use.
DATASET_SOURCE=`echo $SOURCE | sed 's/:/ /g' | awk '{print $2}'`	# e.g. pool/data/data
DATASET_TARGET=`echo $TARGET | sed 's/:/ /g' | awk '{print $2}'`	# e.g. pool/data/data

SCRIPT_NAME=`basename $0`
SSHUSER="root"
LOGFILE="$(dirname "$0")/$(basename "$0").log"
SSHKEY="$(dirname "$0")/id_rsa"
EXCLUDE_DATASETS="$(dirname "$0")/exclude-datasets.txt"
NO_SNAP=0 # disable create snapshots
FULL=0 # рекурсивная реплика
TEST=0 # no send
SSHFLAGS="-i $SSHKEY -oStrictHostKeyChecking=no"
#sshcmd="ssh $SSHUSER@$HOST_TARGET $SSHFLAGS"
SNAPSHOTMARK="REPLICATION-"
LATEST_SNAPSHOT_SOURCE="$SNAPSHOTMARK`date +%F_%T`"
LATEST_SNAPSHOT_TARGET=""
EXCLUDE_LIST=""

#set -e #causes the shell to exit if any subcommand or pipeline returns a non-zero status

# -------------------
#   HELPERS SECTION
# -------------------

inArray() {
	local e
	local localArray="${2}"
	for e in $localArray; do 
		[ "$e" == "$1" ] && return 1; done
	return 0
}

# -------------------

print_help() {
	echo "Replication Script"
	echo ""
	echo "Usage: $SCRIPT_NAME options..."
	echo "Параметры:"
	echo "  exclude=./file.txt          Файл со списком исключаемых датасетов [опционально]"
	echo "  full                        Использовать -R для полной реплики и -I для инкрементальной [опционально]"
	echo "  nosnap                      Не создавать снапшоты до и после репликации [опционально]"
	echo "  test                        Тестовый режим (без запуска команд репликации) [опционально]"
	echo ""
}

read_arguments() {
	# Если скрипт запущен без аргументов, открываем справку.
	if [ $# = 0 ]; then
		print_help
		exit
	fi
	# Парсим аргументы
	for ARG in "$@"
	do
		if [ $ARG = "nosnap" ]; then
			echo $ARG
			NO_SNAP="yes"; fi
		if [ $ARG = "test" ]; then
			echo "test mode"
			TEST="yes"; fi
		if [ $ARG = "full" ]; then
			FULL="yes"; fi

		case "$ARG" in
			exclude*) read_exclude ${ARG##*=} ; # exclude=file.txt
		esac
	done

	# пробуем прочитать файл по-умолчанию
	if [ ! "$EXCLUDE_LIST" ]; then
		read_exclude "$EXCLUDE_DATASETS"
	fi
}

#---CHECK PROCESS ALREADY RUNING---#
PID_FILENAME="${*//\//_}" #replace / in filename
PID_FILE="$0 $PID_FILENAME.pid"

if [ -f "$PID_FILE" ]
then
    PID=$(<"$PID_FILE") #read pid value from file into variable

    if ps -p $PID > /dev/null
    then
        echo "script already runing, exit"
        exit
    else
        echo "script not runing, but pidfile exist, delete pidfile"
        rm -f -- '$PID_FILE'
    fi
else
    echo "script is not runing, continue"
fi

# Ensure PID file is removed on program exit.
trap "rm -f -- '$PID_FILE'" EXIT

# Create a file with current PID to indicate that process is running.
echo $$ > "$PID_FILE"
#---END CHECK---#

read_exclude() {
	local filePath=$1

	if [ -f "$filePath" ]; then
		EXCLUDE_LIST=`cat $filePath`
	fi
}

log() {
	if [ "$1" ];
	then
		echo "`date "+%Y.%m.%d %H:%M"` | $1"
		#echo "`date "+%Y.%m.%d %H:%M"` | $1">>${LOGFILE}
	fi
}

check_error() {
	if [ "$1" ];
	then
		if [ "$1" = "255" ]; 
		then
			#log "ZFS ERROR, $2, EXIT script"
			log "ZFS ERROR, $2"
			#exit
		else
			log "ZFS OK, $2"
		fi
	fi
}

sshcmd() {
	echo "ssh $SSHUSER@$1 $SSHFLAGS"
}

ssh_wraper() {
	# Wrap to ssh if required
	if [ $2 == "local" ]; then
		echo "$1"
	else
		echo "`sshcmd "$2"` \"$1\""
	fi
}

snapshoting() {
	if [ $NO_SNAP == "yes" ]; then
		log "[Snapshoting source = NO]"
		return;
	fi

	local source=$1
	local snapshotMark="$LATEST_SNAPSHOT_SOURCE"
	local recursion="-r"

	if [ "$2" ]; 
	then
		recursion=""
		snapshotMark="$2`date +%F_%T`"
	fi

	log "[Snapshoting source = START]"
	eval $(ssh_wraper "zfs snapshot $recursion $source@$snapshotMark" "$HOST_SOURCE") && check_error "0" "Snapshoting"  || check_error "255" "Snapshoting"
	log "[Snapshoting source = DONE]"
}

find_latest_snapshots() {
	local source=$1
	local target=$2

	if [ $NO_SNAP == "yes" ]; then
		log "Finding latest snapshots = START"
		LATEST_SNAPSHOT_SOURCE=`eval $(ssh_wraper "zfs list -H -o name -s creation -t snapshot -r -d 1 $source | grep $SNAPSHOTMARK | tail -n 1" "$HOST_SOURCE")`
		LATEST_SNAPSHOT_SOURCE=${LATEST_SNAPSHOT_SOURCE##*@}
	fi

	log "Snapshot Source (latest) = $LATEST_SNAPSHOT_SOURCE"

	LATEST_SNAPSHOT_TARGET=`eval $(ssh_wraper "zfs list -H -o name -s creation -t snapshot -r -d 1 $target | tail -n 1" "$HOST_TARGET")`
	LATEST_SNAPSHOT_TARGET=${LATEST_SNAPSHOT_TARGET##*@}
	log "Snapshot Target (latest) = $LATEST_SNAPSHOT_TARGET"
}

check_is_pool() {
	_text=$1
	_check=`echo $_text | grep -o '[/]\+' | head -1`
	_contains=${#_check}
	if [ 0 -eq ${_contains} ]; then
		return 0;
	else
		return 1;
	fi
}


zfs_rollback() {
	local target=$1
	eval $(ssh_wraper "zfs rollback $target@$LATEST_SNAPSHOT_TARGET" "$HOST_TARGET")
}

zfs_inherit_target () {
	local source=$1
	local target=$2

	if check_is_pool $source; then
		log "Target is Pool, not Dataset. Do mountpoint inherit"
		zfs inherit -r mountpoint $target;
	fi
}

zfs_send(){
	local source=$1
	local target=$2

	local r=""
	local i="-i"

	if [ $FULL == "yes" ]; then 
		r="-R"
		i="-I"
	fi

	if [ $TEST == "yes" ]; then return; fi;
	if [ "`eval $(ssh_wraper "zfs list | grep $target" "$HOST_TARGET")`" ]; then
		log "ZFS Rollback" 
		zfs_rollback "$target"
		log "Starting incremental replication"
		eval $(ssh_wraper "zfs send $i $LATEST_SNAPSHOT_TARGET $source@$LATEST_SNAPSHOT_SOURCE" "$HOST_SOURCE") | eval $(ssh_wraper "zfs receive -vu $target" "$HOST_TARGET") && check_error "0" "Incremental Replication"  || check_error "255" "Incremental Replication"
	else
		log "Starting FULL replication"
		eval $(ssh_wraper "zfs send $r $source@$LATEST_SNAPSHOT_SOURCE" "$HOST_SOURCE") | eval $(ssh_wraper "zfs receive -vu $target" "$HOST_TARGET") && check_error "0" "FULL Replication"  || check_error "255" "FULL Replication"
		#zfs_inherit_target "$source" "$target"
	fi
}

source_is_excluded() {
	local name=$1

	if [ ! "$EXCLUDE_LIST" ]; then
		return 0
	fi

	inArray "$name" "${EXCLUDE_LIST}"
	return $?
}

validate_target(){
	# у нас нет имени последнего снапшота в премнике - проходим валидацию
	if [ ! "$LATEST_SNAPSHOT_TARGET" ]; then
		return 1
	fi

	local source=$1
	local target="$source@${LATEST_SNAPSHOT_TARGET}"
	log "Validate Target [START]"
	#log "TGT $target"

	local sourceSnapshots=`eval $(ssh_wraper "zfs list -H -t snapshot -o name -r -d 1 $source" "$HOST_SOURCE")`
	#log "SRC $sourceSnapshots"
	echo ""

	inArray "$target" "${sourceSnapshots}"
	return $?
}

validate_root() {
	if [ "`eval $(ssh_wraper "zfs list | grep $DATASET_TARGET" "$HOST_TARGET")`" ]; then
		local LATEST_SNAPSHOT_TARGET=`eval $(ssh_wraper "zfs list -H -o name -s creation -t snapshot -r -d 1 $DATASET_TARGET | tail -n 1" "$HOST_TARGET")`
		local LATEST_SNAPSHOT_TARGET=${LATEST_SNAPSHOT_TARGET##*@}

		log "Validate ROOT"
		validate_target "$DATASET_SOURCE"
		return $?
	fi

	return 1;
}

replication() {
	echo ""
	echo "Replication of $1"

	local target=$DATASET_TARGET${1##*$DATASET_SOURCE} # ${1##*$DATASET_SOURCE} берем первый аргумент и вырезаем из него $DATASET_SOURCE
	local source=$1

	source_is_excluded $source
	local needExclude=$?

	if [ "$needExclude" == "1" ]; then
		log "$source exсluded"
		echo " "
		return;
	fi

	#check parent
	local PARENT=`eval $(ssh_wraper "zfs get -H -o value origin $source" "$HOST_SOURCE")`

	if [ "$PARENT" != "-" ]; then
		echo ""
		PARENT=`echo "$PARENT" | cut -f1 -d@`
		log "Start PARENT replication | $PARENT"
		replication $PARENT
		log "Finish PARENT replication | $PARENT"
		echo ""
	fi

	echo "dataset target is | $target" 
	echo "dataset source is | $source"
	echo " "

	find_latest_snapshots "$source" "$target"

	validate_target "$source"
	local validated=$?

	if [ "$validated" != "1" ]; then
		log "Initial snapshot (LATEST_SNAPSHOT_TARGET) is missing on DATASET_SOURCE"
		return;
	else
		log "LATEST_SNAPSHOT_TARGET is found in source"
	fi

	if [ "$LATEST_SNAPSHOT_SOURCE" != "$LATEST_SNAPSHOT_TARGET" ]; then
		zfs_send "$source" "$target";
		snapshoting "$source" "REPLICATED-"
	else
		log "Latest snapshot already received"
	fi
}

read_arguments "$@"
log "[START SCRIPT]"

validate_root
ROOT_VALID=$?

if [ "$ROOT_VALID" != "1" ]; then
	log "DATASET_TARGET exists but initial snapshot is missing on DATASET_SOURCE"
	exit 0
fi

snapshoting "$DATASET_SOURCE"
SOURCE_DATASETS_LIST=`eval $(ssh_wraper "zfs list -rH -o name $DATASET_SOURCE" "$HOST_SOURCE")`
for i in $SOURCE_DATASETS_LIST; do
	#echo "$i"
	#sleep 2
	replication $i #&
done
log "[END SCRIPT]"
exit
#for HOST in $(< ListOfHosts); do ssh $HOST ’sudo apt-get update’ & done
#Полная репликация сейчас начинается с последнего снапшота, а надо с первого, затем производить инкрементальную репликацию.

#other thinks 
#$SSHCMD "zfs list -H -o name -t snapshot | grep $DATASET_SOURCE@$SNAPSHOTMARK | sort -r | tail -n +3 | xargs -n 1 zfs destroy -r" && check_error "0" "Clear old Snapshot"  || check_error "255" "Clear old Snapshot"
#POOL_TARGET=`echo $DATASET_TARGET | grep -o '[^/]\+' | head -1`
#DATASET_NAME=`echo $LATEST_SNAPSHOT | grep -o "^.*@" | grep -o ".*[^@]"`
#SNAP_NAME=`echo $LATEST_SNAPSHOT | grep -o "@.*" | grep -o "[^@].*"`

# nosnap + full replication не совместимы = исключение
# проверка пришедшего датасета на inherit, исправление и повторная проверка
# проверка что последние снапшоты корректны.
# Копировать конфигурацию в папку adm
#  zfs allow -l zfsreplicator snapshot,hold,send pool0
#  zfs allow -d zfsreplicator snapshot,hold,send pool0
#nohup command1 > /dev/null 2>&1 &
#nohup command2 >> /path/to/command2.log 2>&1 &
# zfs list -H -r -t snapshot -o name pool0 | grep | xargs -n 1 echo
#http://habrahabr.ru/post/31590/
# http://wiki.bash-hackers.org/syntax/pe#substring_removal